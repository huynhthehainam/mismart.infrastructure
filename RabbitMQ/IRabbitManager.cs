using System;

namespace MiSmart.Infrastructure.RabbitMQ
{
    public interface IRabbitManager
    {
        void Publish<T>(T message, String? exchangeName = null, String? exchangeType = null, String? routeKey = null)
            where T : class;
    }
}