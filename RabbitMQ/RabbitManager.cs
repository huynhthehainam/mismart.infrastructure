using System;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using MiSmart.Infrastructure.Constants;
using RabbitMQ.Client;

namespace MiSmart.Infrastructure.RabbitMQ
{
    public class RabbitManager : IRabbitManager
    {
        private readonly DefaultObjectPool<IModel> objectPool;
        private readonly RabbitOptions rabbitOptions;

        public RabbitManager(IPooledObjectPolicy<IModel> objectPolicy, IOptions<RabbitOptions> options)
        {
            objectPool = new DefaultObjectPool<IModel>(objectPolicy, Environment.ProcessorCount * 2);
            this.rabbitOptions = options.Value;
        }

        public void Publish<T>(T message, String? exchangeName = null, String? exchangeType = null, String? routeKey = null)
            where T : class
        {
            exchangeName = exchangeName ?? rabbitOptions.ExchangeName;
            exchangeType = exchangeType ?? rabbitOptions.ExchangeType;
            routeKey = routeKey ?? rabbitOptions.RouteKey;
            if (message == null)
                return;

            var channel = objectPool.Get();

            // try
            // {
            //     channel.ExchangeDeclare(exchangeName, exchangeType, true, false, null);

            //     var sendBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message));

            //     var properties = channel.CreateBasicProperties();
            //     properties.Persistent = true;

            //     channel.BasicPublish(exchangeName, routeKey, properties, sendBytes);
            // }
            // catch (Exception ex)
            // {
            //     throw ex;
            // }
            // finally
            // {
            //     objectPool.Return(channel);
            // }

            channel.ExchangeDeclare(exchangeName, exchangeType, true, false, null);

            var sendBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message, JsonSerializerDefaultOptions.CamelOptions));

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;

            channel.BasicPublish(exchangeName, routeKey, properties, sendBytes);
            objectPool.Return(channel);
        }
    }
}