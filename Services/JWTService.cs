using System;
using System.Linq;
using System.Text;
using System.Security.Claims;
using MiSmart.Infrastructure.Constants;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using MiSmart.Infrastructure.Settings;
using MiSmart.Infrastructure.ViewModels;

namespace MiSmart.Infrastructure.Services
{
    public class JWTService
    {
        private AuthSettings authSettings;
        public JWTService(IOptions<AuthSettings> options1)
        {
            this.authSettings = options1.Value;
        }
        public UserCacheViewModel? GetUser(String token)
        {

            var validator = new JwtSecurityTokenHandler();
            var jwtToken = validator.ReadJwtToken(token);
            var userClaim = jwtToken.Claims.FirstOrDefault(ww => ww.Type == Keys.JWTAuthKey);
            var adminClaim = jwtToken.Claims.FirstOrDefault(ww => ww.Type == Keys.JWTAdminKey);
            var uuidClaim = jwtToken.Claims.FirstOrDefault(ww => ww.Type == Keys.JWTUUIDKey);
            var roleClaim = jwtToken.Claims.FirstOrDefault(ww => ww.Type == Keys.JWTRoleKey);
            var emailClaim = jwtToken.Claims.FirstOrDefault(ww => ww.Type == Keys.JWTEmailKey);
            var type = jwtToken.Claims.FirstOrDefault(ww => ww.Type == Keys.JWTUserTypeKey);

            if (userClaim is not null)
            {
                return new UserCacheViewModel
                {
                    ID = Convert.ToInt64(userClaim.Value),
                    RoleID = roleClaim is null ? 0 : Convert.ToInt32(roleClaim.Value),
                    IsAdmin = adminClaim is null ? false : Convert.ToBoolean(adminClaim.Value),
                    Type = type is null ? "User" : Convert.ToString(type.Value),
                    UUID = uuidClaim is null ? Guid.Empty : Guid.Parse(uuidClaim.Value),
                    Email = emailClaim is null ? "" : Convert.ToString(emailClaim.Value),
                    IsActive = true,
                };
            }
            return null;
        }
        public String GenerateAccessToken(UserCacheViewModel user, DateTime accessTokenExpiration)
        {
            var claims = new[] {
                new Claim(Keys.JWTAuthKey, user.ID.ToString()),
                new Claim(Keys.JWTAdminKey, user.IsAdmin.ToString()),
                new Claim(Keys.JWTRoleKey, user.RoleID.ToString()),
                new Claim(Keys.JWTUUIDKey, user.UUID.ToString()),
                new Claim(Keys.JWTUserTypeKey, "User"),
                new Claim(Keys.JWTEmailKey, user.Email ?? ""),
             };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authSettings.AuthSecret ?? ""));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(claims: claims, expires: accessTokenExpiration, signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}