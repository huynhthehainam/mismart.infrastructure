using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Minio;
using MiSmart.Infrastructure.Helpers;
using System;
using System.IO;
using System.Threading.Tasks;

namespace MiSmart.Infrastructure.Minio
{
    public class MinioService
    {
        public MinioSettings Settings { get; set; }
        private MinioClient minioClient;
        public MinioService(IOptions<MinioSettings> options)
        {
            this.Settings = options.Value;
            this.minioClient = new MinioClient().WithEndpoint(Settings.Hostname).WithCredentials(Settings.AccessKey, Settings.SecretKey).Build();

        }
        public async Task<String> PutFileAsync(IFormFile file, String[] paths)
        {
            var randomFileName = FileHelpers.GenerateRandomFileName(file);
            var temp = String.Join("/", paths);
            var path = $"{temp}/{randomFileName}";


            PutObjectArgs putObjectArgs = new PutObjectArgs().WithBucket(Settings.BucketName).WithObject(path).WithStreamData(file.OpenReadStream()).WithObjectSize(file.Length);
            await minioClient.PutObjectAsync(putObjectArgs);
            return $"https://{Settings.Hostname}/{Settings.BucketName}/{path}";
        }
        public String GetFilePathFromUrl(String fileUrl)
        {
            var prefix = $"https://{Settings.Hostname}/{Settings.BucketName}/";
            if (!fileUrl.StartsWith(prefix))
            {
                throw new Exception("Minio service does not contains this url");
            }
            var path = fileUrl.Substring(prefix.Length);
            return path;
        }
        public async Task RemoveFileByUrlAsync(String fileUrl)
        {
            var path = GetFilePathFromUrl(fileUrl);
            RemoveObjectArgs removeObjectArgs = new RemoveObjectArgs().WithBucket(Settings.BucketName).WithObject(path);
            await minioClient.RemoveObjectAsync(removeObjectArgs);
        }
        public async Task<Byte[]> DownloadFileAsync(String fileUrl)
        {
            var path = GetFilePathFromUrl(fileUrl);
            var ms = new MemoryStream();
            GetObjectArgs getObjectArgs = new GetObjectArgs().WithBucket(Settings.BucketName).WithObject(path).WithCallbackStream(s => s.CopyTo(ms));
            await minioClient.GetObjectAsync(getObjectArgs);
            return ms.ToArray();

        }
    }
}