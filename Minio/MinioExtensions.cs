using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MiSmart.Infrastructure.Minio
{
    public static class MinioExtensions
    {
        public static void AddMinio(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<MinioSettings>(configuration.GetSection("MinioSettings"));
            services.AddSingleton<MinioService, MinioService>();
        }
    }
}