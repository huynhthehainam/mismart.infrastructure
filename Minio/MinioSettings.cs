




using System;

namespace MiSmart.Infrastructure.Minio
{
    public class MinioSettings
    {
        public String? Hostname { get; set; }
        public String? BucketName { get; set; }
        public String? AccessKey { get; set; }
        public String? SecretKey { get; set; }
    }
}