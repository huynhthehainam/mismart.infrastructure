using System.Linq;
using System.Text.Json;
using MiSmart.Infrastructure.Constants;
using MiSmart.Infrastructure.ViewModels;
using Microsoft.AspNetCore.Authorization;
using MiSmart.Infrastructure.Responses;
using System;

namespace MiSmart.Infrastructure.Controllers
{
    [Authorize]
    public abstract class AuthorizedAPIControllerBase : APIControllerBase
    {
        protected AuthorizedAPIControllerBase(IActionResponseFactory actionResponseFactory) : base(actionResponseFactory)
        {
        }
        public UserCacheViewModel CurrentUser
        {
            get
            {
                var userClaims = HttpContext.User;
                if (userClaims.Claims.FirstOrDefault(ww => ww.Type == Keys.IdentityClaim) != null)
                {

                    var claim = userClaims.Claims.FirstOrDefault(ww => ww.Type == Keys.IdentityClaim);
                    if (claim is not null)
                    {
                        String claimsString = claim.Value;
                        var vm = JsonSerializer.Deserialize<UserCacheViewModel>(claimsString);
                        if (vm is not null && vm.Type == "User")
                            return vm;
                    }
                }
                ActionResponse actionResponse = actionResponseFactory.CreateInstance();
                actionResponse.AddAuthorizationErr();

                throw new Exception("Cannot found current user");
            }
        }
    }
}