using System;

namespace MiSmart.Infrastructure.Constants
{
    public class DefaultValues
    {
        public const String AvatarUrl = "https://minio.s3.mismart.ai/trung-mua/auth-storage/default/avatar.jpg";
    }
}