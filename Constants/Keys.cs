using System;

namespace MiSmart.Infrastructure.Constants
{
    public class Keys
    {
        public const String AuthHeaderKey = "Authorization";
        public const String JWTPrefixKey = "Bearer";
        public const String JWTAuthKey = "sub";
        public const String JWTRoleKey = "RoleMiSmart";
        public const String JWTAdminKey = "AdminMiSmart";
        public const String JWTUserTypeKey = "UserTypeMiSmart";
        public const String JWTUUIDKey = "UUIDMiSmart";
        public const String JWTEmailKey = "EmailMiSmart";
        public const String AllowedOrigin = "AllowedOrigin";
        public const String IdentityClaim = "Auth";
    }
}