using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MiSmart.Infrastructure.Commands;
using MiSmart.Infrastructure.Responses;
using MiSmart.Infrastructure.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace MiSmart.Infrastructure.Repositories
{
    public abstract class RepositoryBase<T> where T : class
    {
        protected DbContext context { get; set; }
        protected RepositoryBase(DbContext context) => this.context = context;
        public async Task<T?> GetAsync(Expression<Func<T, Boolean>> expression)
        {
            return await context.Set<T>().FirstOrDefaultAsync(expression);
        }
        public virtual async Task<TView?> GetViewAsync<TView>(Expression<Func<T, Boolean>> expression) where TView : class, IViewModel<T>, new()
        {
            var entity = await context.Set<T>().FirstOrDefaultAsync(expression);
            return entity is not null ? ViewModelHelpers.ConvertToViewModel<T, TView>(entity) : null;
        }
        public virtual async Task<ListResponse<TView>> GetListResponseViewAsync<TView>(PageCommand pageCommand, Expression<Func<T, Boolean>> expression, Func<T, Object>? order = null, Boolean ascending = true) where TView : class, IViewModel<T>, new()
        {
            var count = await context.Set<T>().CountAsync(expression);
            List<TView> data;
            var pageIndex = pageCommand.PageIndex;
            var pageSize = pageCommand.PageSize;
            var originData = context.Set<T>().Where(expression);
            if (pageIndex.HasValue && pageSize.HasValue)
            {

                if (order is Object)
                {
                    data = ascending ? originData.OrderBy(order).Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList().Select(ww => ViewModelHelpers.ConvertToViewModel<T, TView>(ww)).ToList() :
                    originData.OrderByDescending(order).Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList().Select(ww => ViewModelHelpers.ConvertToViewModel<T, TView>(ww)).ToList();
                }
                else
                {
                    data = originData.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList().Select(ww => ViewModelHelpers.ConvertToViewModel<T, TView>(ww)).ToList();
                }
            }
            else
            {

                if (order is Object)
                {
                    data = ascending ? originData.OrderBy(order).ToList().Select(ww => ViewModelHelpers.ConvertToViewModel<T, TView>(ww)).ToList() :
                  originData.OrderByDescending(order).ToList().Select(ww => ViewModelHelpers.ConvertToViewModel<T, TView>(ww)).ToList();
                }
                else
                {
                    data = originData.ToList().Select(ww => ViewModelHelpers.ConvertToViewModel<T, TView>(ww)).ToList();
                }
            }
            return new ListResponse<TView> { Data = data, PageIndex = pageIndex, PageSize = pageSize, TotalRecords = count };
        }
        public virtual Task<List<T>> GetListEntitiesAsync(PageCommand pageCommand, Expression<Func<T, Boolean>> expression, Func<T, Object>? order = null, Boolean ascending = true)
        {
            List<T> data;
            var pageIndex = pageCommand.PageIndex;
            var pageSize = pageCommand.PageSize;
            var originData = context.Set<T>().Where(expression);
            if (pageIndex.HasValue && pageSize.HasValue)
            {
                if (order is Object)
                {
                    data = ascending ? originData.OrderBy(order).Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList() :
                    originData.OrderByDescending(order).Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList();
                }
                else
                {
                    data = originData.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList();
                }
            }
            else
            {

                if (order is Object)
                {
                    data = ascending ? originData.OrderBy(order).ToList() :
            originData.OrderByDescending(order).ToList();
                }
                else
                {
                    data = originData.ToList();
                }
            }
            return Task.FromResult(data);
        }
        public virtual async Task<Boolean> AnyAsync(Expression<Func<T, Boolean>> expression)
        {
            return await context.Set<T>().AnyAsync(expression);
        }
        public virtual async Task<ListResponse<T>> GetListAsync(PageCommand pageCommand, Expression<Func<T, Boolean>> expression, Func<T, Object>? order = null, Boolean ascending = true)
        {
            var taskCount = context.Set<T>().CountAsync(expression);
            List<T> data;
            var pageIndex = pageCommand.PageIndex;
            var pageSize = pageCommand.PageSize;
            var originData = context.Set<T>().Where(expression);
            if (pageIndex.HasValue && pageSize.HasValue)

            {
                if (order is Object)
                {
                    data = ascending ? originData.OrderBy(order).Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList() :
                    originData.OrderByDescending(order).Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList();
                }
                else
                {
                    data = originData.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList();
                }
            }
            else
            {

                if (order is Object)
                {
                    data = ascending ? originData.OrderBy(order).ToList() :
            originData.OrderByDescending(order).ToList();
                }
                else
                {
                    data = originData.ToList();
                }
            }
            var count = await taskCount;
            return new ListResponse<T> { Data = data, PageIndex = pageIndex, PageSize = pageSize, TotalRecords = count };
        }
        public virtual async Task<T> CreateAsync(T entity)
        {
            await context.AddAsync(entity);
            await context.SaveChangesAsync();
            return entity;
        }
        public virtual async Task<T> UpdateAsync(T entity)
        {
            context.Set<T>().Update(entity);
            await context.SaveChangesAsync();
            return entity;
        }
        public virtual async Task DeleteAsync(T entity)
        {
            context.Set<T>().Remove(entity);
            await context.SaveChangesAsync();
        }
        public virtual async Task DeleteRangeAsync(List<T> listEntities)
        {
            context.RemoveRange(listEntities);
            await context.SaveChangesAsync();
        }
    }
}