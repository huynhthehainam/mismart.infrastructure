using System;

namespace MiSmart.Infrastructure.Settings
{
    public class KeySettings
    {
        public String? AuthCacheKey { get; set; }
    }
}