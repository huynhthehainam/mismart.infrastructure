using System;

namespace MiSmart.Infrastructure.Settings
{
    public class ExpiredTimeSettings
    {
        public Int32 AccessTokenExpirationTime { get; set; }
    }
}