using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using MiSmart.Infrastructure.Constants;
namespace MiSmart.Infrastructure.ViewModels
{
    public class UserCacheViewModel
    {
        public String? Username { get; set; }
        public String? Email { get; set; }
        public Boolean IsAdmin { get; set; } = false;
        public Int32 RoleID { get; set; } =0;
        public Int64 ID { get; set; }=0;
        public Guid UUID { get; set; } = Guid.Empty;
        public String? Type { get; set; }
        public Boolean IsActive { get; set; } =  false;
        public List<Int32>? AllowedFunctionIds { get; set; }
        public UserCacheViewModel() { }
        public static UserCacheViewModel? GetUserCache(ClaimsPrincipal userClaims)
        {
            UserCacheViewModel? user = null;
            var claims = userClaims.Claims.FirstOrDefault(ww => ww.Type == Keys.IdentityClaim);
            if (claims != null)
            {
                user = JsonSerializer.Deserialize<UserCacheViewModel>(claims.Value);
            }
            return user;
        }
        public Boolean IsAdministrator
        {
            get => (IsAdmin || RoleID == 1);
        }
    }
}