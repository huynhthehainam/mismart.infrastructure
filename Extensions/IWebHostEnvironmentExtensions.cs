using System;
using Microsoft.AspNetCore.Hosting;

namespace MiSmart.Infrastructure.Extensions;
public static class IWebHostEnvironmentExtensions
{
    public static Boolean IsTesting(this IWebHostEnvironment env)
    {
        return env.EnvironmentName == "Testing";
    }
}